//
//  TestExnessAPI.swift
//  TestExnessAPI
//
//  Created by Nikita Zhukov on 3/12/18.
//  Copyright © 2018 home. All rights reserved.
//

import XCTest
import SwiftWebSocket
@testable import TestExness

class TestExnessAPI: XCTestCase {
    
    let TIMEOUT_FOR_UNIVERCE = 10.0
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testOpenCloseConnection() {
        let _expectation = expectation(description: "connect to server")
        
        let ws = WebSocket()
        
        ws.event.open = { [unowned ws] in
            print("### connection opened")
            ws.close()
        }
        ws.event.close = { code, reason, clean in
            print("### connection closed reason: \(reason)")
            _expectation.fulfill()
        }
        ws.event.error = { error in
            XCTAssert(false, "### connection down with error: \(error)")
        }
        
        ws.open(nsurl: Constants().webSocketAddress())
        wait(for: [_expectation], timeout: TIMEOUT_FOR_UNIVERCE)
    }
    
    func testSubscribeThenClose() {
        let _expectation = expectation(description: "check message")
        
        let ws = WebSocket()
        
        ws.event.open = {
            print("### connection opened")
        }
        ws.event.message = { [unowned ws] message in
            print("### connection message: \(message)")
            ws.close()
        }
        ws.event.close = { code, reason, clean in
            print("### connection closed reason: \(reason)")
            _expectation.fulfill()
        }
        
        ws.open(nsurl: Constants().webSocketAddress())
        ws.send(SubscribtionRequest(symbols: [Symbol(value:"EURUSD", isSubscribed:true)], action: .subscribe).make())
        
        wait(for: [_expectation], timeout: TIMEOUT_FOR_UNIVERCE)
    }
    
    func testSubscribeUnsubscribeThenClose() {
        let _expectation = expectation(description: "check message")
        
        var flowCount = 0
        
        let ws = WebSocket()
        
        let parceAndUnsubscribe = { [unowned ws] (_ message:Any) in
            if let textMsg = message as? String, let data = textMsg.data(using: .utf8) {
                do {
                    let responce = try JSONDecoder().decode(SubscribtionResponce.self, from: data)
                    
                    if let count = responce.subscribed_count, count == 1 {
                        flowCount += 1
                        ws.send(SubscribtionRequest(symbols: [Symbol(value:"EURUSD", isSubscribed:false)], action: .unsubscribe).make())
                    } else {
                        flowCount += 1
                        ws.close()
                    }
                } catch {
                    XCTAssert(false, "### responce parcing failed with error: \(error)")
                }
            }
        }
        
        let checkFlow = {
            XCTAssert(flowCount == 2, "### flow failed")
        }
        
        ws.event.open = {
            print("### connection opened")
        }
        ws.event.message = {message in
            print("### connection message: \(message)")
            parceAndUnsubscribe(message)
        }
        ws.event.close = { code, reason, clean in
            print("### connection closed reason: \(reason)")
            checkFlow()
            _expectation.fulfill()
        }
        
        ws.open(nsurl: Constants().webSocketAddress())
        ws.send(SubscribtionRequest(symbols: [Symbol(value:"EURUSD", isSubscribed:true)], action: .subscribe).make())
        
        wait(for: [_expectation], timeout: TIMEOUT_FOR_UNIVERCE)
    }
    
    
}
