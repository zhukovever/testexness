//
//  Constants.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/12/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation

struct Constants {
    public let DB_NAME = "db.sqlite3"
    
    public let WEB_SOCKET_URL = "wss://quotes.exness.com"
    public let WEB_SOCKET_PORT = 18400
    
    public func webSocketAddress() -> URL {
        guard var components = URLComponents(string: WEB_SOCKET_URL)
            else { fatalError("invalid WEB_SOCKET_URL") }
        
        components.port = WEB_SOCKET_PORT
        
        guard let url = components.url
            else { fatalError("invalid url components") }
        
        return url
    }
    
    public func dbPath() -> String {
        guard let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first else { fatalError("invalid path to Documents dir") }
        let fullPath = "\(path)/\(DB_NAME)"
        print("### connection path :\(fullPath)")
        return fullPath
    }
}

