//
//  SubscribedQuotesStorage.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/14/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation
import SQLite

class SubscribedQuotesStorage: Any {

    init(connection:Connection) {
        self.connection = connection
    }
    
    private var connection:Connection
    
    func updateQuote(quotes:[Quotes]?) {
        guard let _quotes = quotes else { return }
        
        _quotes.forEach {
            do {
                if let _symbol = $0.symbol, let _ask = $0.ask, let _bid = $0.bid, let _spread = $0.spread {
                    try connection.run(Quotes.table.insert(or: .replace,
                                                           Quotes.symbol <- _symbol,
                                                           Quotes.ask <- _ask,
                                                           Quotes.bid <- _bid,
                                                           Quotes.spread <- _spread))
                }
            } catch {
                print("### quote insert or replace failed: \(error)")
            }
        }
    }
    
    
    func retrieveFor(symbols:[Symbol], _ callback: (([Quotes]) -> Void)) {
        let symbolNames = symbols.flatMap { return $0.value }
        if let seq = try? connection.prepare(Quotes.table.filter(symbolNames.contains(Quotes.symbol))) {
            var array:[Quotes] = []
            seq.forEach{ row in
                array.append(Quotes(symbol: row[Quotes.symbol], bid: row[Quotes.bid], ask: row[Quotes.ask], spread: row[Quotes.spread]))
            }
            callback(array)
        } else {
            callback([])
        }
        
    }
    
}
