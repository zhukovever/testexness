//
//  Quotes.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import SQLite

struct Ticks: Codable {
    public var ticks:[Quotes]?
}

struct Quotes: Codable {

    public var symbol:String?
    public var bid:String?
    public var ask:String?
    public var spread:String?
    
    enum CodingKeys: String, CodingKey {
        case symbol = "s"
        case bid = "b"
        case ask = "a"
        case spread = "spr"
    }
    
    static let table = Table("Quotes")
    static let symbol = Expression<String>("symbol")
    static let bid = Expression<String>("bid")
    static let ask = Expression<String>("ask")
    static let spread = Expression<String>("spread")
    
}
