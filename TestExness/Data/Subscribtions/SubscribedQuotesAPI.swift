//
//  SubscribedQuotesAPI.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit
import SwiftWebSocket

class SubscribedQuotesAPI: Any {
    
    private var reconnectionHelper:ReconnectionHelper
    private var ws:WebSocket
    private let wsQueue:DispatchQueue
    private var shouldBeOpened:Bool {
        didSet {
            if shouldBeOpened {
                ws.open(nsurl: Constants().webSocketAddress())
            } else {
                if ws.readyState != .closed {
                    ws.close()
                }
            }
        }
    }
    
    public var symbolsStorage:SymbolsStorage?
    public var quotesStorage:SubscribedQuotesStorage?
    
    public var retrieveSubscribtions:(([Quotes]?) -> Void)?
    public var retrieveQuotes:(([Quotes]?) -> Void)?
    
    init() {
        reconnectionHelper = ReconnectionHelper()
        
        ws = WebSocket()
        wsQueue = DispatchQueue(label: "com.testexness.websocket", qos: DispatchQoS.userInteractive)
        ws.eventQueue = wsQueue
        shouldBeOpened = false
        
        ws.event.open = { [weak self] in
            print("### socket raised")
            self?.reconnectionHelper.resetReconnectionTime()
            self?.subscribeToStoredSymbols()
        }
        ws.event.end = { [weak self] (code, reason, wasClean, error) in
            print("### socket down")
            guard let _shouldBeOpened = self?.shouldBeOpened, _shouldBeOpened else {
                return
            }
            guard let helper = self?.reconnectionHelper else { return }
            self?.wsQueue.asyncAfter(wallDeadline: .now() + helper.reconnectAfter, execute: {
                self?.ws.open()
            })
            self?.reconnectionHelper.increaseReconnectionTime()
        }
        ws.event.error = { error in
            print("### error: \(error)")
        }
        ws.event.message = { [weak self] message in
            print("### connection message: \(message)")
            self?.parseAndNotify(message: message)
        }
        
    }
    
    func start() {
        shouldBeOpened = true
    }
    
    func finish() {
        shouldBeOpened = false
    }
    
    func updateSubscriptionFor(symbol:Symbol) {
        if let isSubscribed = symbol.isSubscribed, isSubscribed {
            subscribeTo(symbols: [symbol])
        } else {
            unsubscribeFrom(symbols: [symbol])
        }
    }
    
    func subscribeTo(symbols:[Symbol]) {
        symbolsStorage?.updateSubscribtion(symbols: symbols)
        ws.send(SubscribtionRequest(symbols: symbols, action: .subscribe).make())
    }
    
    func unsubscribeFrom(symbols:[Symbol]) {
        symbolsStorage?.updateSubscribtion(symbols: symbols)
        ws.send(SubscribtionRequest(symbols: symbols, action: .unsubscribe).make())
    }
    
    private func subscribeToStoredSymbols() {
        symbolsStorage?.retrieve({ [weak self] (symbols) in
            self?.subscribeTo(symbols: symbols.filter { $0.isSubscribed ?? false })
        })
    }
    
    private func parseAndNotify(message:Any) {
        if let textMsg = message as? String, let data = textMsg.data(using: .utf8) {
            do {
                let subscribtionResponce = try JSONDecoder().decode(SubscribtionResponce.self, from: data)
                if subscribtionResponce.subscribed_count != nil {
                    updateSubscribtion(subscribtionResponce)
                    return
                }
            
                let quotesResponce = try JSONDecoder().decode(Ticks.self, from: data)
                if quotesResponce.ticks != nil {
                    updateQuotes(quotesResponce)
                    return
                }
                
            } catch {
                print("### responce parcing failed with error: \(error)")
            }
        }
    }
    
    private func updateSubscribtion(_ responce:SubscribtionResponce) {
        DispatchQueue.main.async { [weak self] in
            if let retrieveSubscribtions = self?.retrieveSubscribtions {
                self?.quotesStorage?.updateQuote(quotes: responce.subscribed_list?.ticks)
                retrieveSubscribtions(responce.subscribed_list?.ticks)
            }
        }
    }
    
    private func updateQuotes(_ responce:Ticks) {
        DispatchQueue.main.async { [weak self] in
            if let retrieveQuotes = self?.retrieveQuotes {
                self?.quotesStorage?.updateQuote(quotes: responce.ticks)
                retrieveQuotes(responce.ticks)
            }
        }
    }
}
