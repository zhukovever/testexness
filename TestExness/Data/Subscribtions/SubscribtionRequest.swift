//
//  SubscribtionRequest.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/12/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation

enum SubscribtionAction:String {
    case subscribe
    case unsubscribe
}

struct SubscribtionRequest {
    public var symbols:[Symbol]
    public var action:SubscribtionAction?
    
    func make() -> String {
        let requestedSymbol = symbols.reduce("") { (result, symbol) -> String in
            if let value = symbol.value {
                var _result = result
                if _result != "" {
                    _result += ","
                }
                _result += value
                return _result
            }
            return result
        }
        
        if let _action = action?.rawValue, requestedSymbol != "" {
            return "\(_action): \(requestedSymbol)".uppercased()
        }
        return ""
    }
}
