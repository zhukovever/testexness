//
//  SymbolsStorage.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation
import SQLite

class SymbolsStorage {
    
    init(connection:Connection) {
        self.connection = connection
    }
    
    private var connection:Connection
    
    private var symbols:[Symbol] {
        get {
            var array = select()
            if array.count == 0 {
                array = generateInitialList()
                insertOrReplace(symbols: array)
            }
            return array
        }
        set {
            insertOrReplace(symbols: symbols)
        }
    }
    
    func updateSubscribtion(symbols:[Symbol]) {
        insertOrReplace(symbols: symbols)
    }
    
    func retrieve(_ callback: (([Symbol]) -> Void)) {
        callback(symbols)
    }
    
    
    private func generateInitialList() -> [Symbol] {
        let symbolStrList = ["EURUSD", "EURGBP", "USDJPY", "GBPUSD", "USDCHF", "USDCAD", "AUDUSD", "EURJPY", "EURCHF", ]
        return symbolStrList.map{ Symbol(value: $0, isSubscribed: false) }
    }
    
    private func select() -> [Symbol] {
        if let seq = try? connection.prepare(Symbol.table) {
            var array:[Symbol] = []
            seq.forEach{ row in
                array.append(Symbol(value: row[Symbol.value], isSubscribed: row[Symbol.isSubscribed]))
            }
            return array
        }
        return []
    }
    
    private func insertOrReplace(symbols:[Symbol]) {
        symbols.forEach {
            do {
                if let _value = $0.value, let _isSubscribed = $0.isSubscribed {
                    try connection.run(Symbol.table.insert(or: .replace,
                                                           Symbol.value <- _value,
                                                           Symbol.isSubscribed <- _isSubscribed))
                }
            } catch {
                print("### symbol insert or replace failed: \(error)")
            }
        }
    }
    
}
