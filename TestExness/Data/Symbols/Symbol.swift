//
//  Symbol.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation
import SQLite

struct Symbol {
    
    public var value:String?
    public var isSubscribed:Bool?
 
    static let table = Table("Symbols")
    static let id = Expression<Int>("id")
    static let value = Expression<String>("value")
    static let isSubscribed = Expression<Bool>("isSubscribed")
    
}
