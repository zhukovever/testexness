//
//  ReconnectionHelper.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/15/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class ReconnectionHelper: Any {

    private var reconnectionTimeIdx = 0
    private var reconnectionTimeArray = [0.0, 1.0, 4.0, 8.0]
    
    public var reconnectAfter:Double {
        get {
            return reconnectionTimeArray[reconnectionTimeIdx]
        }
    }
    
    public func increaseReconnectionTime() {
        if reconnectionTimeIdx + 1 < reconnectionTimeArray.count {
            reconnectionTimeIdx += 1
        }
    }
    
    public func resetReconnectionTime() {
        reconnectionTimeIdx = 0
    }
    
    
}
