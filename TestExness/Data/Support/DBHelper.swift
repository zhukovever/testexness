//
//  DBHelper.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/15/18.
//  Copyright © 2018 home. All rights reserved.
//

import Foundation
import SQLite

class DBHelper: Any {

    public var connection:Connection!
    
    init() {
        makeConnection()
        makeDB()
    }
    
    func makeConnection() {
        connection = try! Connection(Constants().dbPath())
    }
    
    func makeDB() {
        
        _ = try? connection.run(Symbol.table.create(block: { (t) in
            t.column(Symbol.id, primaryKey: true)
            t.column(Symbol.value, unique: true)
            t.column(Symbol.isSubscribed, defaultValue:false)
        }))
        
        _ = try? connection.run(Quotes.table.create(block: { (t) in
            t.column(Quotes.symbol, unique: true)
            t.column(Quotes.bid)
            t.column(Quotes.ask)
            t.column(Quotes.spread)
        }))
    
    }
    
    
}
