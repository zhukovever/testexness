//
//  QuotesDataAdapter.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/11/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class QuotesDataAdapter: NSObject {
    
    static let SORTED_SYMBOL_KEY = "SORTED_SYMBOL_KEY"
    
    typealias CellType = QuotesCell
    
    var didDeleteSymbol:((_ symbolName:String)->())?
    
    weak var tableView:UITableView? {
        didSet {
            let cellName = String(describing:QuotesCell.self)
            tableView?.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        }
    }
    
    public var initialArray:[Quotes]? {
        didSet {
            let _array:[Quotes]!
            if initialArray != nil {
                _array = initialArray
            } else {
                _array = []
            }
            
            quotesBinding = extractBindingsFrom(array: _array)
            symbolList = extractSortedSymbolsFrom(bindings: quotesBinding)
            saveSortOf(symbols: symbolList)
            
            tableView?.reloadData()
        }
    }
    public var updateArray:[Quotes]? {
        didSet {
            guard let _array = updateArray else { return }
            
            let bindings = extractBindingsFrom(array: _array)
            
            quotesBinding.merge(bindings) { (q1, q2) -> Quotes in
                return q2
            }
            
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: QuotesCell.QUOTES_CELL_UPDATE_KEY), object: nil, userInfo: bindings)
        }
    }
    
    private var symbolList:[String] = []
    private var quotesBinding:[String:Quotes] = [:]
    
    private func extractBindingsFrom(array:[Quotes]) -> [String:Quotes] {
        return array.reduce([:]) { (dict, quote) in
            var _dict = dict
            if let symbol = quote.symbol {
                _dict[symbol] = quote
            }
            return _dict
        }
    }
    private func extractSortedSymbolsFrom(bindings:[String:Quotes]) -> [String] {
        let newSymbolList = Array(bindings.keys)
        
        let ud = UserDefaults.standard
        var _symbolList = ud.array(forKey: QuotesDataAdapter.SORTED_SYMBOL_KEY) as? [String] ?? []
        
        _symbolList = _symbolList.filter { newSymbolList.contains($0) }
        _symbolList.append(contentsOf: newSymbolList.filter { !_symbolList.contains($0) })
        return _symbolList
    }
    
    private func saveSortOf(symbols:[String]) {
        let ud = UserDefaults.standard
        ud.set(symbols, forKey: QuotesDataAdapter.SORTED_SYMBOL_KEY)
        ud.synchronize()
    }
}


extension QuotesDataAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symbolList.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let nib = Bundle.main.loadNibNamed(String(describing: QuotesHeaderView.self), owner: nil, options: nil)?.first
        return nib as? QuotesHeaderView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:CellType.self), for:indexPath) as? CellType
            else { fatalError("make cell failed") }
        
        cell.quote = quotesBinding[symbolList[indexPath.row]]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCellEditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = symbolList[sourceIndexPath.row]
        symbolList.remove(at: sourceIndexPath.row)
        symbolList.insert(movedObject, at: destinationIndexPath.row)
        saveSortOf(symbols: symbolList)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if let _didDeleteSymbol = didDeleteSymbol, editingStyle == .delete {
            let symbolName = symbolList[indexPath.row]
            symbolList = symbolList.filter { $0 != symbolName }
            saveSortOf(symbols: symbolList)
            self.tableView?.deleteRows(at: [indexPath], with: .automatic)
            _didDeleteSymbol(symbolName)
        }
    }
}
