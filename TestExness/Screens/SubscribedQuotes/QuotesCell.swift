//
//  QuotesCell.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/11/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

extension Quotes {
    func symbolTitle() -> String {
        if let _symbol = symbol {
            if _symbol.count == 6 {
                let splitPart = _symbol.index(_symbol.startIndex, offsetBy: 3)
                return _symbol[..<splitPart] + "/" + _symbol[splitPart..<_symbol.endIndex]
            } else {
                return _symbol
            }
        }
        return "-"
    }
    func askBidTitle() -> String {
        let _ask = ask ?? "-"
        let _bid = bid ?? "-"
        return "\(_ask) / \(_bid)"
    }
    func spreadTitle() -> String {
        let _spread = spread ?? "-"
        return "\(_spread)"
    }
}

class QuotesCell: UITableViewCell {

    static let QUOTES_CELL_UPDATE_KEY = "QUOTES_CELL_UPDATE_KEY"
    
    @IBOutlet private weak var labelSymbol: UILabel!
    @IBOutlet private weak var labelAskBid: UILabel!
    @IBOutlet private weak var labelSpread: UILabel!
    
    
    public var quote:Quotes! {
        didSet {
            labelSymbol.text = quote.symbolTitle()
            labelAskBid.text = quote.askBidTitle()
            labelSpread.text = quote.spreadTitle()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateQuote(_:)), name: NSNotification.Name(rawValue: QuotesCell.QUOTES_CELL_UPDATE_KEY), object: nil)
    }
    
    @objc func updateQuote(_ notification: NSNotification) {
        if let passedData = notification.userInfo as? [String:Quotes],
            let symbol = self.quote.symbol,
            let quote = passedData[symbol] {
            self.quote = quote
        }
    }
    
}
