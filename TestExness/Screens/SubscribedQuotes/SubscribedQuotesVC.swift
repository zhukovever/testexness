//
//  SubscribedQuotesVC.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class SubscribedQuotesVC: BaseViewController {
    
    public var subscribedQuotesAPI:SubscribedQuotesAPI!
    public var subscribedQuotesDataAdapter:QuotesDataAdapter!
    
    @IBOutlet private var barButtonEdit: UIBarButtonItem!
    @IBOutlet private var barButtonManage: UIBarButtonItem!
    @IBOutlet private var barButtonDone: UIBarButtonItem!
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupNavbar()
        setupTableView()
    }
    
    func setupNavbar() {
        if tableView.isEditing {
            navigationItem.leftBarButtonItem = nil
            navigationItem.rightBarButtonItem = barButtonDone
        } else {
            navigationItem.leftBarButtonItem = barButtonManage
            navigationItem.rightBarButtonItem = barButtonEdit
        }
        navigationItem.rightBarButtonItem?.isEnabled = false
        navigationItem.rightBarButtonItem?.isEnabled = true
    }

    func setupTableView() {
        
        subscribedQuotesDataAdapter.tableView = tableView
        tableView.delegate = subscribedQuotesDataAdapter
        tableView.dataSource = subscribedQuotesDataAdapter
        
        subscribedQuotesAPI.symbolsStorage?.retrieve({ [weak self] (symbols) in
            subscribedQuotesAPI.quotesStorage?.retrieveFor(symbols: symbols.filter { $0.isSubscribed ?? false }, { (quotes) in
                self?.subscribedQuotesDataAdapter.initialArray = quotes
            })
        })
        subscribedQuotesDataAdapter.didDeleteSymbol = { [weak self] (symbolName) in
            self?.subscribedQuotesAPI.unsubscribeFrom(symbols: [Symbol(value: symbolName, isSubscribed: false)])
        }
        subscribedQuotesAPI.retrieveSubscribtions = { [weak self] (quotes) in
            self?.subscribedQuotesDataAdapter.initialArray = quotes
        }
        subscribedQuotesAPI.retrieveQuotes = { [weak self] (quotes) in
            self?.subscribedQuotesDataAdapter.updateArray = quotes
        }
    }

    @IBAction private func manageHandler(_ sender: Any) {
        appCoordinator?.showManageSymbols()
    }
    
    @IBAction private func editHandler(_ sender: Any) {
        tableView.isEditing = true
        setupNavbar()
    }
    
    @IBAction private func doneHandler(_ sender: Any) {
        tableView.isEditing = false
        setupNavbar()
    }
    
}
