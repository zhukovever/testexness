//
//  ManageSymbolsCell.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class ManageSymbolsCell: UITableViewCell {

    @IBOutlet private weak var labelSymbolName: UILabel!
    @IBOutlet private weak var switchControl: UISwitch!
    
    public var symbol:Symbol! {
        didSet {
            labelSymbolName.text = symbol.value
            switchControl.isOn = symbol.isSubscribed ?? false
        }
    }
    public var didSelectSymbol:((_ symbol:Symbol)->())!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
    }
    
    @IBAction private func switchHandler(_ sender: Any) {
        if let _didSelectSymbol = didSelectSymbol {
            symbol.isSubscribed = switchControl.isOn
            _didSelectSymbol(symbol)
        } else {
            assertionFailure("callback must be defined")
        }
    }
    
}
