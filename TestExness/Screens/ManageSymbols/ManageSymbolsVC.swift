//
//  ManageSymbolsVC.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class ManageSymbolsVC: BaseViewController {
    
    public var subscribedQuotesAPI:SubscribedQuotesAPI!
    public var symbolsStorage:SymbolsStorage!
    public var symbolsDataAdapter:SymbolsDataAdapter!
    
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private var barButtonClose: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupNavbar()
        setupTableView()
    }
    
    func setupNavbar() {
        navigationItem.leftBarButtonItem = barButtonClose
    }
    
    func setupTableView() {
        symbolsDataAdapter.tableView = tableView
        tableView.delegate = symbolsDataAdapter
        tableView.dataSource = symbolsDataAdapter
        
        symbolsStorage.retrieve { [weak self] symbols in
            self?.symbolsDataAdapter.initialArray = symbols
        }
        symbolsDataAdapter.didSelectSymbol = { [weak self] symbol in
            self?.subscribedQuotesAPI.updateSubscriptionFor(symbol: symbol)
        }
    }
    
    @IBAction private func closeHandler(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
