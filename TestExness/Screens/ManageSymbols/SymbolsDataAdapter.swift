//
//  ManageSymbolsDataAdapter.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit


class SymbolsDataAdapter: NSObject {

    typealias CellType = ManageSymbolsCell
    
    var didSelectSymbol:((_ symbol:Symbol)->())?
    
    weak var tableView:UITableView? {
        didSet {
            let cellName = String(describing:CellType.self)
            tableView?.register(UINib(nibName: cellName, bundle: nil), forCellReuseIdentifier: cellName)
        }
    }
    
    var initialArray:[Symbol]? = nil {
        didSet {
            if let _array = initialArray {
                symbolsBindings = _array.reduce([:]) { (dict, symbol) in
                    var _dict = dict
                    if let value = symbol.value {
                        _dict[value] = symbol
                    }
                    return _dict
                }
                symbolsList = Array(symbolsBindings.keys)
            }
            
            self.tableView?.reloadData()
        }
    }
    
    var symbolsList:[String] = []
    var symbolsBindings:[String:Symbol] = [:]
    
}

extension SymbolsDataAdapter: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return symbolsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing:CellType.self), for:indexPath) as? CellType
            else { fatalError("make cell failed") }
        
        
        cell.symbol = symbolsBindings[symbolsList[indexPath.row]]
        cell.didSelectSymbol = { [weak self] symbol in
            if let _self = self, let _didSelectSymbol = _self.didSelectSymbol {
                _self.symbolsBindings[_self.symbolsList[indexPath.row]] = symbol
                _didSelectSymbol(symbol)
            }
        }
        
        return cell
    }
}

