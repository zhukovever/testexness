//
//  BaseViewController.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    public weak var appCoordinator:AppCoordinator?
    
}
