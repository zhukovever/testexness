//
//  AppCoordinator.swift
//  TestExness
//
//  Created by Nikita Zhukov on 3/10/18.
//  Copyright © 2018 home. All rights reserved.
//

import UIKit

class AppCoordinator {
    
    private let window: UIWindow
    private let subscribedQuotesApi: SubscribedQuotesAPI
    private let symbolsStorage:SymbolsStorage
    
    let navigationController:UINavigationController = {
        let nvc = UINavigationController()
        return nvc
    }()
    
    init(window: UIWindow) {
        let dbHelper = DBHelper()
        
        self.window = window
        subscribedQuotesApi = SubscribedQuotesAPI()
        symbolsStorage = SymbolsStorage(connection: dbHelper.connection)
        subscribedQuotesApi.symbolsStorage = symbolsStorage
        subscribedQuotesApi.quotesStorage = SubscribedQuotesStorage(connection: dbHelper.connection)
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        subscribedQuotesApi.start()
    }
    
    private func extractViewController<T:BaseViewController>(by type:T.Type) -> T {
        let vcName = String(describing: type.self)
        guard let vc = UIStoryboard(name: vcName, bundle: nil).instantiateViewController(withIdentifier: vcName) as? T
            else { fatalError("make \(vcName) failed") }
        
        return vc
    }
    
    func showSubscribedSymbols() {
        let vc = extractViewController(by: SubscribedQuotesVC.self)
        vc.appCoordinator = self
        vc.subscribedQuotesAPI = subscribedQuotesApi
        vc.subscribedQuotesDataAdapter = QuotesDataAdapter()
        
        navigationController.viewControllers = [vc]
    }
    
    func showManageSymbols() {
        let vc = extractViewController(by: ManageSymbolsVC.self)
        vc.appCoordinator = self
        vc.symbolsStorage = self.symbolsStorage
        vc.symbolsDataAdapter = SymbolsDataAdapter()
        vc.subscribedQuotesAPI = subscribedQuotesApi
        let nvc = UINavigationController(rootViewController: vc)
        
        navigationController.present(nvc, animated: true, completion: nil)
    }
    
}
